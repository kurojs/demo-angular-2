export class Customer {
  Id: number;
  Fullname: string;
  AvartarURL: string;
  NRIC: string;
  Email: string;
  Password: string;
  Gender: number;
  DOB: Date;
  Phone: string;
  status: number;
  Profesional: string;
  Star: number;
  IsDeleted: boolean;
  lastModifiedDate: Date;
}
