import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.css']
})
export class CustomerAddComponent implements OnInit {
  customer: Customer;

  constructor(
    private customerService: CustomerService,
    private location: Location
  ) {}

  ngOnInit() {
    this.customer = new Customer();
  }

  goBack(): void {
    this.location.back();
  }

  add(customer): void {
    console.log(customer);
    this.customerService
      .addCustomer(customer as Customer)
      .subscribe(() => this.goBack());
  }
}
