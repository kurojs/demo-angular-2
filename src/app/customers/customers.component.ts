import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {
  customers: Customer[];

  constructor(private customerService: CustomerService) {}

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers(): void {
    this.customerService.getCustomers().subscribe(customers => {
      this.customers = customers;
      console.log(this.customers);
    });
  }

  search(term: string): void {
    this.customerService.searchCustomersByName(term).subscribe(customers => {
      this.customers = customers;
      console.log(this.customers);
    });
  }

  // add(customer: Customer): void {
  //   this.customerService.addCustomer(customer).subscribe(_customer => {
  //     this.customers.push(_customer);
  //   });
  // }

  delete(customer: Customer): void {
    this.customers = this.customers.filter(_customer => _customer !== customer);
    this.customerService.deleteCustomer(customer).subscribe();
  }
}
